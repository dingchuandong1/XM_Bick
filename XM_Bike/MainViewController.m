//
//  MainViewController.m
//  XM_Bike
//
//  Created by dcd on 16/9/30.
//  Copyright © 2016年 XM. All rights reserved.
//

#import "MainViewController.h"
#import "YQSlideMenuController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(push:)];
    self.navigationItem.rightBarButtonItem = rightItem;
    //    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    //    imageView.image = [UIImage imageNamed:@"slide_bg"];
    //    [self.view addSubview:imageView];
    //
    //    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    //    animation.fromValue = [NSValue valueWithCGPoint:CGPointMake(-self.view.bounds.size.width, 0)];
    //    animation.toValue = [NSValue valueWithCGPoint:CGPointMake(self.view.bounds.size.width, 0)];
    //    animation.repeatCount = INT32_MAX;
    //    animation.duration = 3.0f;
    //    [imageView.layer addAnimation:animation forKey:@"animation"];
    self.view.backgroundColor = [UIColor colorWithRed:arc4random()%255/255.0 green:arc4random()%255/255.0 blue:arc4random()%255/255.0 alpha:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
