//
//  AppDelegate.h
//  XM_Bike
//
//  Created by dcd on 16/9/30.
//  Copyright © 2016年 XM. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AVFoundation/AVFoundation.h>
@protocol LoginDelegate <NSObject>
@optional
- (void)lognSuccess;
@end
@interface AppDelegate : UIResponder <UIApplicationDelegate,LoginDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, assign) BOOL location;

@end

